﻿using Demo.Api.Resources;
using FluentValidation;

namespace Demo.Api.Validators
{
    public class SaveAuthorResourceValidator : AbstractValidator<SaveAuthorResource>
    {
        public SaveAuthorResourceValidator()
        {
            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
