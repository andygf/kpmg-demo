﻿namespace Demo.Api.Resources
{
    public class SaveBookResource
    {
        public string Name { get; set; }
        public int AuthorId { get; set; }
    }
}
