﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Demo.Api.Resources
{
    public class CollectionResource<T> : ICollection
    {
        private readonly ArrayList _items = new ArrayList();

        public string CollectionName { get; set; }

        public int Count => _items.Count;

        public object SyncRoot => this;

        public bool IsSynchronized => false;

        public T this[int index] => (T) _items[index];

        public CollectionResource(){ }

        public CollectionResource(IEnumerable<T> data)
        {
            _items.AddRange(data.ToArray());
        }

        public void CopyTo(Array a, int index)
        {
            _items.CopyTo(a, index);
        }

        public IEnumerator GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public void Add(T newItem)
        {
            _items.Add(newItem);
        }
    }
}
