﻿using AutoMapper;
using Demo.Api.Resources;
using Demo.Api.Validators;
using Demo.Core.Models;
using Demo.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Demo.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly ILogger<BooksController> _logger;
        private readonly IBookService _bookService;
        private readonly IMapper _mapper;

        public BooksController(IBookService bookService, IMapper mapper, ILogger<BooksController> logger)
        {
            _bookService = bookService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookResource>>> GetAllBooks()
        {
            var books = await _bookService.GetAllWithAuthor();
            var bookResources = _mapper.Map<IEnumerable<Book>, IEnumerable<BookResource>>(books);

            return Ok(bookResources);
        }

        [HttpGet("xml")]
        public async Task<ActionResult<IEnumerable<AuthorResource>>> GetAllAuthorsXml([FromQuery] string xpath = null)
        {
            var books = await _bookService.GetAllWithAuthor();
            var collection = new CollectionResource<BookResource>(_mapper.Map<IEnumerable<Book>, IEnumerable<BookResource>>(books))
            {
                CollectionName = "Books"
            };

            var doc = new XmlDocument();
            var nav = doc.CreateNavigator();
            using (var w = nav.AppendChild())
            {
                var ser = new XmlSerializer(typeof(CollectionResource<BookResource>));
                ser.Serialize(w, collection);
            }

            if (string.IsNullOrWhiteSpace(xpath)) return Ok(doc.OuterXml);

            try
            {
                var nodes = doc.SelectNodes(xpath);
                return Ok((from XmlNode node in nodes select node.OuterXml).ToList());
            }
            catch (XPathException e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BookResource>> GetBookById(int id)
        {
            var book = await _bookService.GetBookById(id);
            var bookResource = _mapper.Map<Book, BookResource>(book);

            return Ok(bookResource);
        }

        [HttpPost]
        public async Task<ActionResult<BookResource>> CreateBook([FromBody] SaveBookResource saveBookResource)
        {
            var validator = new SaveBookResourceValidator();
            var validationResult = await validator.ValidateAsync(saveBookResource);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors); // this needs refining, but for demo it is ok

            var bookToCreate = _mapper.Map<SaveBookResource, Book>(saveBookResource);

            var newBook = await _bookService.CreateBook(bookToCreate);

            var book = await _bookService.GetBookById(newBook.Id);

            var bookResource = _mapper.Map<Book, BookResource>(book);

            return Ok(bookResource);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<BookResource>> UpdateBook(int id, [FromBody] SaveBookResource saveBookResource)
        {
            var validator = new SaveBookResourceValidator();
            var validationResult = await validator.ValidateAsync(saveBookResource);

            var requestIsInvalid = id == 0 || !validationResult.IsValid;

            if (requestIsInvalid)
                return BadRequest(validationResult.Errors); // this needs refining, but for demo it is ok

            var bookToBeUpdate = await _bookService.GetBookById(id);

            if (bookToBeUpdate == null)
                return NotFound();

            var book = _mapper.Map<SaveBookResource, Book>(saveBookResource);

            await _bookService.UpdateBook(bookToBeUpdate, book);

            var updatedBook = await _bookService.GetBookById(id);
            var updatedBookResource = _mapper.Map<Book, BookResource>(updatedBook);

            return Ok(updatedBookResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            if (id <= 0)
                return BadRequest();

            var book = await _bookService.GetBookById(id);

            if (book == null)
                return NotFound();

            await _bookService.DeleteBook(book);
            _logger.LogInformation("Some logs where needed");

            return NoContent();
        }
    }
}
