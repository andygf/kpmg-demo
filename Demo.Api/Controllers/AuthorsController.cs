﻿using AutoMapper;
using Demo.Api.Resources;
using Demo.Api.Validators;
using Demo.Core.Models;
using Demo.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace Demo.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly ILogger<AuthorsController> _logger;
        private readonly IAuthorService _authorService;
        private readonly IMapper _mapper;

        public AuthorsController(IAuthorService authorService, IMapper mapper, ILogger<AuthorsController> logger)
        {
            this._mapper = mapper;
            this._authorService = authorService;
            _logger = logger;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<AuthorResource>>> GetAllAuthors()
        {
            var authors = await _authorService.GetAllAuthors();
            var authorResources = _mapper.Map<IEnumerable<Author>, IEnumerable<AuthorResource>>(authors);

            return Ok(authorResources);
        }

        [HttpGet("xml")]
        public async Task<ActionResult<IEnumerable<AuthorResource>>> GetAllAuthorsXml([FromQuery] string xpath = null)
        {
            var authors = await _authorService.GetAllAuthors();
            var collection = new CollectionResource<AuthorResource>(_mapper.Map<IEnumerable<Author>, IEnumerable<AuthorResource>>(authors))
            {
                CollectionName = "Books"
            };

            var doc = new XmlDocument();
            var nav = doc.CreateNavigator();
            using (var w = nav.AppendChild())
            {
                var ser = new XmlSerializer(typeof(CollectionResource<BookResource>));
                ser.Serialize(w, collection);
            }

            if (string.IsNullOrWhiteSpace(xpath)) return Ok(doc.OuterXml);

            try
            {
                var nodes = doc.SelectNodes(xpath);
                return Ok((from XmlNode node in nodes select node.OuterXml).ToList());
            }
            catch (XPathException e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AuthorResource>> GetAuthorById(int id)
        {
            var author = await _authorService.GetAuthorById(id);
            var authorResource = _mapper.Map<Author, AuthorResource>(author);

            return Ok(authorResource);
        }

        [HttpPost("")]
        public async Task<ActionResult<AuthorResource>> CreateAuthor([FromBody] SaveAuthorResource saveAuthorResource)
        {
            var validator = new SaveAuthorResourceValidator();
            var validationResult = await validator.ValidateAsync(saveAuthorResource);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors); // this needs refining, but for demo it is ok

            var authorToCreate = _mapper.Map<SaveAuthorResource, Author>(saveAuthorResource);

            var newAuthor = await _authorService.CreateAuthor(authorToCreate);

            var author = await _authorService.GetAuthorById(newAuthor.Id);

            var authorResource = _mapper.Map<Author, AuthorResource>(author);

            return Ok(authorResource);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<AuthorResource>> UpdateAuthor(int id, [FromBody] SaveAuthorResource saveAuthorResource)
        {
            var validator = new SaveAuthorResourceValidator();
            var validationResult = await validator.ValidateAsync(saveAuthorResource);

            if (!validationResult.IsValid)
                return BadRequest(validationResult.Errors); // this needs refining, but for demo it is ok

            var authorToBeUpdated = await _authorService.GetAuthorById(id);

            if (authorToBeUpdated == null)
                return NotFound();

            var author = _mapper.Map<SaveAuthorResource, Author>(saveAuthorResource);

            await _authorService.UpdateAuthor(authorToBeUpdated, author);

            var updatedAuthor = await _authorService.GetAuthorById(id);

            var updatedAuthorResource = _mapper.Map<Author, AuthorResource>(updatedAuthor);

            return Ok(updatedAuthorResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAuthor(int id)
        {
            if (id <= 0)
                return BadRequest(); 

            var author = await _authorService.GetAuthorById(id);

            if (author == null)
                return NotFound();

            await _authorService.DeleteAuthor(author);
            _logger.LogInformation("Some logs where needed");

            return NoContent();
        }
    }
}