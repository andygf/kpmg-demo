﻿using AutoMapper;
using Demo.Api.Resources;
using Demo.Core.Models;

namespace Demo.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to Resource
            CreateMap<Book, BookResource>();
            CreateMap<Author, AuthorResource>();

            // Resource to Domain
            CreateMap<BookResource, Book>();
            CreateMap<AuthorResource, Author>();
            CreateMap<SaveBookResource, Book>();
            CreateMap<SaveAuthorResource, Author>();
        }
    }
}
