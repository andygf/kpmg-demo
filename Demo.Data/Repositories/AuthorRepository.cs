﻿using Demo.Core.Models;
using Demo.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Demo.Data.Repositories
{
    public class AuthorRepository : Repository<Author>, IAuthorRepository
    {
        public AuthorRepository(DemoDbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Author>> GetAllWithBooksAsync()
        {
            return await DemoDbContext.Authors
                .Include(a => a.Books)
                .ToListAsync();
        }

        public Task<Author> GetWithBooksByIdAsync(int id)
        {
            return DemoDbContext.Authors
                .Include(a => a.Books)
                .SingleOrDefaultAsync(a => a.Id == id);
        }

        private DemoDbContext DemoDbContext => Context as DemoDbContext;
    }
}

