﻿using Demo.Core.Models;
using Demo.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Data.Repositories
{

    public class BookRepository : Repository<Book>, IBookRepository
    {
        public BookRepository(DemoDbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Book>> GetAllWithAuthorAsync()
        {
            return await DemoDbContext.Books
                .Include(m => m.Author)
                .ToListAsync();
        }

        public async Task<Book> GetWithAuthorByIdAsync(int id)
        {
            return await DemoDbContext.Books
                .Include(m => m.Author)
                .SingleOrDefaultAsync(m => m.Id == id); 
        }

        public async Task<IEnumerable<Book>> GetAllWithAuthorByAuthorIdAsync(int authorId)
        {
            return await DemoDbContext.Books
                .Include(m => m.Author)
                .Where(m => m.AuthorId == authorId)
                .ToListAsync();
        }

        private DemoDbContext DemoDbContext => Context as DemoDbContext;
    }
}

