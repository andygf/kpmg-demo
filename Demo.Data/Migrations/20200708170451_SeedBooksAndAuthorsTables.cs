﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Demo.Data.Migrations
{
    public partial class SeedBooksAndAuthorsTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql("INSERT INTO Authors (Name) Values ('Mark Twain')");
            migrationBuilder
                .Sql("INSERT INTO Authors (Name) Values ('Stephen King')");

            migrationBuilder
                .Sql("INSERT INTO Books (Name, AuthorId) Values ('The Adventures of Tom Sawyer', (SELECT Id FROM Authors WHERE Name = 'Mark Twain'))");
            migrationBuilder
                .Sql("INSERT INTO Books (Name, AuthorId) Values ('The Gilded Age', (SELECT Id FROM Authors WHERE Name = 'Mark Twain'))");
            migrationBuilder
                .Sql("INSERT INTO Books (Name, AuthorId) Values ('The Adventures of Huckleberry Finn', (SELECT Id FROM Authors WHERE Name = 'Mark Twain'))");
            migrationBuilder
                .Sql("INSERT INTO Books (Name, AuthorId) Values ('Carrie', (SELECT Id FROM Authors WHERE Name = 'Stephen King'))");
            migrationBuilder
                .Sql("INSERT INTO Books (Name, AuthorId) Values ('The Shining', (SELECT Id FROM Authors WHERE Name = 'Stephen King'))");
            migrationBuilder
                .Sql("INSERT INTO Books (Name, AuthorId) Values ('Needful Things', (SELECT Id FROM Authors WHERE Name = 'Stephen King'))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder
                .Sql("DELETE FROM Books");

            migrationBuilder
                .Sql("DELETE FROM Authors");
        }
    }
}
