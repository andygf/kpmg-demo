﻿using Demo.Core.Models;
using Demo.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace Demo.Data
{
    public class DemoDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        public DemoDbContext(DbContextOptions<DemoDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .ApplyConfiguration(new BookConfiguration());

            builder
                .ApplyConfiguration(new AuthorConfiguration());
        }
    }
}
