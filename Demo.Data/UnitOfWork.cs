﻿using Demo.Core;
using Demo.Core.Repositories;
using Demo.Data.Repositories;
using System;
using System.Threading.Tasks;

namespace Demo.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DemoDbContext _context;
        private BookRepository _bookRepository;
        private AuthorRepository _authorRepository;

        public UnitOfWork(DemoDbContext context)
        {
            this._context = context;
        }

        public IBookRepository Books => _bookRepository ??= new BookRepository(_context);

        public IAuthorRepository Authors => _authorRepository ??= new AuthorRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
