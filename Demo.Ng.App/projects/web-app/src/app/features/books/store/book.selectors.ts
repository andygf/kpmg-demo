import { BookState } from './book.reducers';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { selectAll, selectIds } from './book.reducers';

export const bookFeatureSelector = createFeatureSelector<BookState>('books');

export const getAllBooks = createSelector(bookFeatureSelector, selectAll);

export const areBooksLoaded = createSelector(
  bookFeatureSelector,
  (state) => state.booksLoaded
);
