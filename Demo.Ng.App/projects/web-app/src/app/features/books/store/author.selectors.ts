import { AuthorState } from './author.reducers';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { selectAll, selectIds } from './author.reducers';

export const authorFeatureSelector = createFeatureSelector<AuthorState>(
  'authors'
);

export const getAllAuthors = createSelector(authorFeatureSelector, selectAll);

export const areAuthorsLoaded = createSelector(
  authorFeatureSelector,
  (state) => state.authorsLoaded
);
