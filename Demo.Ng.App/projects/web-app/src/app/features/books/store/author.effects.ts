import {
  authorActionTypes,
  authorsLoaded,
  updateAuthor,
} from './author.actions';
import { AuthorService } from '../services/author.service';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { concatMap, map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthorEffects {
  loadAuthors$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authorActionTypes.loadAuthors),
      concatMap(() => this.authorService.getAllAuthors()),
      map((authors) => authorActionTypes.authorsLoaded({ authors }))
    )
  );

  createAuthor$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authorActionTypes.createAuthor),
        concatMap((action) => this.authorService.createAuthor(action.author)),
        tap(() => this.router.navigateByUrl('/authors'))
      ),
    { dispatch: false }
  );

  deleteAuthor$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authorActionTypes.deleteAuthor),
        concatMap((action) => this.authorService.deleteAuthor(action.authorId))
      ),
    { dispatch: false }
  );

  updateAuthor$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authorActionTypes.updateAuthor),
        concatMap((action) =>
          this.authorService.updateAuthor(
            action.update.id,
            action.update.changes
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private authorService: AuthorService,
    private actions$: Actions,
    private router: Router
  ) {}
}
