import { authorActionTypes, authorsLoaded } from './author.actions';
import { Author } from '../model/author.model';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';

export interface AuthorState extends EntityState<Author> {
  authorsLoaded: boolean;
}

export const adapter: EntityAdapter<Author> = createEntityAdapter<Author>();

export const initialState = adapter.getInitialState({
  authorsLoaded: false,
});

export const authorReducer = createReducer(
  initialState,

  on(authorActionTypes.authorsLoaded, (state, action) => {
    return adapter.setAll(action.authors, { ...state, authorsLoaded: true });
  }),

  on(authorActionTypes.createAuthor, (state, action) => {
    return adapter.addOne(action.author, state);
  }),

  on(authorActionTypes.deleteAuthor, (state, action) => {
    return adapter.removeOne(action.authorId, state);
  }),

  on(authorActionTypes.updateAuthor, (state, action) => {
    return adapter.updateOne(action.update, state);
  })
);

export const { selectAll, selectIds } = adapter.getSelectors();
