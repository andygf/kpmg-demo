import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Author } from '../model/author.model';

export const loadAuthors = createAction(
  '[Authors List] Load Authors via Service'
);

export const authorsLoaded = createAction(
  '[Authors Effect] Authors Loaded Successfully',
  props<{ authors: Author[] }>()
);

export const createAuthor = createAction(
  '[Create Author Component] Create Author',
  props<{ author: Author }>()
);

export const deleteAuthor = createAction(
  '[Authors List Operations] Delete Author',
  props<{ authorId: string }>()
);

export const updateAuthor = createAction(
  '[Authors List Operations] Update Author',
  props<{ update: Update<Author> }>()
);

export const authorActionTypes = {
  loadAuthors,
  authorsLoaded,
  createAuthor,
  deleteAuthor,
  updateAuthor,
};
