import { Author } from './author.model';

export interface Book {
  id: string;
  name: string;
  author: Author;
  authorId: string;
}
