import { NgModule } from '@angular/core';

import { BooksRoutingModule } from './books-routing.module';
import { BooksComponent } from './components/books.component';
import { SharedModule } from '../../shared/shared.module';
import { BookService } from './services/book.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { bookReducer } from './store/book.reducers';
import { BookEffects } from './store/book.effects';
import { authorReducer } from './store/author.reducers';
import { AuthorEffects } from './store/author.effects';
import { AuthorService } from './services/author.service';
import { AuthorsComponent } from './components/authors.component';

@NgModule({
  declarations: [BooksComponent, AuthorsComponent],
  imports: [
    SharedModule,
    BooksRoutingModule,
    StoreModule.forFeature('books', bookReducer),
    StoreModule.forFeature('authors', authorReducer),
    EffectsModule.forFeature([BookEffects, AuthorEffects]),
  ],
  providers: [BookService, AuthorService],
})
export class BooksModule {}
