import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Author } from '../model/author.model';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../store/reducers';
import { AuthorService } from '../services/author.service';
import { authorActionTypes } from '../store/author.actions';
import { getAllAuthors } from '../store/author.selectors';
import { Update } from '@ngrx/entity';

@Component({
  selector: 'demo-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss'],
})
export class AuthorsComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'action'];

  @ViewChild(MatTable, { static: true }) table: MatTable<any> | undefined;

  noData: Author[] = [];

  dataSource: MatTableDataSource<Author>;

  author: Author;

  isUpdateActivated = false;
  isAddActivated = false;

  constructor(
    private authorService: AuthorService,
    private store: Store<AppState>
  ) {
    this.dataSource = new MatTableDataSource(this.noData);
    this.author = {} as Author;
  }

  ngOnInit(): void {
    this.store
      .pipe(select(getAllAuthors))
      .subscribe((authors) => this.initializeData(authors));
  }

  ngAfterViewInit(): void {
    this.store.dispatch(authorActionTypes.loadAuthors());
  }

  private initializeData(authors: Author[]): void {
    const data = authors.length ? authors : this.noData;
    this.dataSource = new MatTableDataSource(data);
  }

  deleteAuthor(authorId: string): void {
    this.store.dispatch(authorActionTypes.deleteAuthor({ authorId }));
  }

  showUpdateForm(author: Author): void {
    this.author = { ...author };
    this.isUpdateActivated = true;
    this.isAddActivated = false;
  }

  showAddForm(): void {
    this.author = {} as Author;
    this.isUpdateActivated = false;
    this.isAddActivated = true;
  }

  updateAuthor(updateForm: any): void {
    const update: Update<Author> = {
      id: this.author?.id || '0',
      changes: {
        ...this.author,
        ...updateForm.value,
      },
    };

    this.store.dispatch(authorActionTypes.updateAuthor({ update }));

    this.isUpdateActivated = false;
    this.author = {} as Author;
  }

  addAuthor(addForm: any): void {
    const author: Author = {
      ...this.author,
      ...addForm.value,
    };

    this.store.dispatch(authorActionTypes.createAuthor({ author }));

    this.isAddActivated = false;
    this.author = {} as Author;
  }
}
