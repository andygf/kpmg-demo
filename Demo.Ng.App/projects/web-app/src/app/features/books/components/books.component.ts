import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../store/reducers';
import { BookService } from '../services/book.service';
import { bookActionTypes } from '../store/book.actions';
import { getAllBooks } from '../store/book.selectors';
import { Update } from '@ngrx/entity';
import { Book } from '../model/book.model';
import { Author } from '../model/author.model';
import { getAllAuthors } from '../store/author.selectors';
import { authorActionTypes } from '../store/author.actions';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'demo-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'name', 'author', 'action'];

  @ViewChild(MatTable, { static: true }) table: MatTable<any> | undefined;

  noData: Book[] = [];

  dataSource: MatTableDataSource<Book>;

  authors: Author[] = [];
  authorControl = new FormControl();

  book: Book;

  isUpdateActivated = false;
  isAddActivated = false;

  constructor(
    private bookService: BookService,
    private store: Store<AppState>
  ) {
    this.dataSource = new MatTableDataSource(this.noData);
    this.book = {} as Book;
  }

  ngOnInit(): void {
    this.store
      .pipe(select(getAllBooks))
      .subscribe((books) => this.initializeData(books));
    this.store
      .pipe(select(getAllAuthors))
      .subscribe((authors) => (this.authors = authors));
  }

  ngAfterViewInit(): void {
    this.store.dispatch(authorActionTypes.loadAuthors());
    this.store.dispatch(bookActionTypes.loadBooks());
  }

  private initializeData(books: Book[]): void {
    const data = books.length ? books : this.noData;
    this.dataSource = new MatTableDataSource(data);
  }

  deleteBook(bookId: string): void {
    this.store.dispatch(bookActionTypes.deleteBook({ bookId }));
  }

  showUpdateForm(book: Book): void {
    this.book = { ...book };
    this.authorControl.setValue(this.book);
    this.isUpdateActivated = true;
    this.isAddActivated = false;
  }

  showAddForm(): void {
    this.book = {} as Book;
    this.authorControl.setValue(this.book);
    this.isUpdateActivated = false;
    this.isAddActivated = true;
  }

  updateBook(updateForm: any): void {
    const book = {
      ...this.book,
      ...updateForm.value,
    };
    book.authorId = book.author.id;

    const update: Update<Book> = {
      id: this.book?.id || '0',
      changes: book,
    };

    this.store.dispatch(bookActionTypes.updateBook({ update }));

    this.isUpdateActivated = false;
    this.book = {} as Book;
  }

  addBook(addForm: any): void {
    const book: Book = {
      ...this.book,
      ...addForm.value,
    };
    book.authorId = book.author.id;

    this.store.dispatch(bookActionTypes.createBook({ book }));

    this.isAddActivated = false;
    this.book = {} as Book;
  }
}
