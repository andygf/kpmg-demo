import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../model/book.model';
import { Author } from '../model/author.model';

@Injectable()
export class BookService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAllAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>('http://localhost:51544/api/authors');
  }

  getAllBooks(): Observable<Book[]> {
    return this.http.get<Book[]>('http://localhost:51544/api/books');
  }

  createBook(book: Book): Observable<Book> {
    return this.http.post<Book>('http://localhost:51544/api/books', book);
  }

  deleteBook(bookId: string): Observable<any> {
    return this.http.delete('http://localhost:51544/api/books/' + bookId);
  }

  updateBook(bookId: string | number, changes: Partial<Book>): Observable<any> {
    return this.http.put('http://localhost:51544/api/books/' + bookId, changes);
  }
}
