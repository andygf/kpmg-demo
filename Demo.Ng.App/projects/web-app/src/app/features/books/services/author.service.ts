import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Author } from '../model/author.model';

@Injectable()
export class AuthorService {
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  getAllAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>('http://localhost:51544/api/authors');
  }

  createAuthor(author: Author): Observable<Author> {
    return this.http.post<Author>('http://localhost:51544/api/authors', author);
  }

  deleteAuthor(authorId: string): Observable<any> {
    return this.http.delete('http://localhost:51544/api/authors/' + authorId);
  }

  updateAuthor(
    authorId: string | number,
    changes: Partial<Author>
  ): Observable<any> {
    return this.http.put(
      'http://localhost:51544/api/authors/' + authorId,
      changes
    );
  }
}
