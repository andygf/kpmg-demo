﻿using Demo.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace Demo.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IBookRepository Books { get; }
        IAuthorRepository Authors { get; }
        Task<int> CommitAsync();
    }
}
