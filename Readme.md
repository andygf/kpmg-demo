# Demo
Repository Pattern and Unit of Work

## Demo.Core
Business logic structure

## Demo.Data
Communicate with the database.
Home of Migrations and Seed Data

## Demo.Services
Business logic

## Demo.Api
Presentation layer
Swagger, FluentValidation, AutoMapper

## Demo.Ng.App
Angular Api client app
Eager part: AppModule, top level routes, and CoreModule
Lazy part: features. lazy modules import SharedModule

### core
main-layout
application wide singleton services
application startup

### features
Lazy features: specific component which can NOT really be re-used by other features.

### shared
Common elements used in different features (components, directives and pipes)
No implement any services (`/core` folder and use `providedIn: 'root'`)

## TODO: (not enough time)
Better UX/UI
Add ap `proxy.conf.json` to remove the hardcoded url
General refactor
Dockerfiles and CI/CD
More Xml Features (lot of experience in my previous project :) )